import {ADD_TODO,DELETE_TODO,TOGGLE_TODO} from '../constants';
import { filter, map } from 'lodash';
import { v4 } from 'uuid';  
 
const todos = (state = [],action) => {
	let todos = null;
	switch(action.type){
		case ADD_TODO:
			
			todos =[
				...state,
				{
					newTodo:action.payload.newTodo,
					todoId:v4(),
					toggled:false,	
				}
			];
			return todos;
			
		case DELETE_TODO:
			todos= state.filter(todos => todos.todoId!==action.payload.todoId);
			return todos;
			
		case TOGGLE_TODO:
					todos = state.map(todo => {
						if(todo.todoId!==action.payload.todoId){
							return todo;
						}
						return  {
							newTodo:todo.newTodo,
							toggled:!todo.toggled,
							todoId:todo.todoId,
						};
					});
					return todos;
		default:
			return state;
	}
}

export default todos;
