import {ADD_TODO,DELETE_TODO,TOGGLE_TODO } from '../constants';

export const addTodo = (todoItem) => {
	const action = {
		type: ADD_TODO,	
		payload:{
			newTodo:todoItem.newTodo,
		}
	}
	return action;
}

export const deleteTodo = (todoId) => {
	const action = {
		type:DELETE_TODO,
		payload:{
			todoId
		}
	}
	return action;
}

export const toggleTodo = (todoId) =>{
	const action ={
		type:TOGGLE_TODO,
		payload:{
			todoId,
		}
	}
	return action;
} 
