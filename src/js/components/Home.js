import React from 'react';
import { connect } from 'react-redux'; 
import { bindActionCreators } from 'redux';
import { addTodo ,deleteTodo,toggleTodo } from '../actions';
import { filter, map } from 'lodash';

class Home extends React.Component {
	constructor(props) {
		super(props);
		this.state={
			text:''
		};
	}	

	render() { 
		let todoComponents = map(this.props.todos, (todo, index)=>{ 
			return (
				<li key={index}>
							<p  style={{textDecoration: todo.toggled? 'line-through':'none' }}>{todo.newTodo}</p>			 
							<button onClick={ ()=> this.props.toggleTodo(todo.todoId)}> toggle </button>  			
							<button onClick={ ()=> this.props.deleteTodo(todo.todoId)}>  remove</button>
				</li>
			);
		});
		
		return (
			<div className="App">
				<input placeholder="I have to ..."  ref="txtTodoName" onChange={ event =>this.setState({ text:event.target.value})}    />
				<button type="button" onClick={()=> this.props.addTodo({newTodo:this.state.text}) }>Add Todo</button>
				<div>
					<ul>
						{ todoComponents } 
					</ul>
				</div>   					  
			</div>
		);
    }
}  

function mapDispatchToProps(dispatch){
	return bindActionCreators({addTodo,toggleTodo,deleteTodo},dispatch);
}

function mapStateToProps(state){
	return {
		todos:state
	}
}
 
export default  connect(mapStateToProps,mapDispatchToProps)(Home);
